package com.dollardays.common;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage extends BaseTest {

	public static final int Page_Load_Timeout = 20;

	public static final int Implicit_Wait = 10;

	public void verifyElementpresent(WebElement ele) {
		WebDriverWait w = new WebDriverWait(driver, Implicit_Wait);

		try {
			w.until(ExpectedConditions.visibilityOf(ele));
			System.out.println(ele + " was found");

		}

		catch (Exception e) {
			System.out.println("Unable to find the element " + ele + ": " + e);

		}

	}

}