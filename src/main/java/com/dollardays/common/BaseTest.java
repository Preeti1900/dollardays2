package com.dollardays.common;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.dollardays.weblocators.SignInPage;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {
	public static WebDriver driver;
	private  SignInPage signInPage;
	private ExtentReports extentReport;

	public void initialization() throws IOException {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();

		String baseUrl = CommonUtilities.getPropertyValue("url");

		driver.get(baseUrl);

		driver.manage().timeouts().pageLoadTimeout(BasePage.Page_Load_Timeout, TimeUnit.SECONDS);

		driver.manage().timeouts().implicitlyWait(BasePage.Implicit_Wait, TimeUnit.SECONDS);

		driver.manage().window().maximize();

		driver.manage().deleteAllCookies();
	}
	public void signInMethod() throws IOException 
	{  
		this.signInPage = new SignInPage(driver);
        this.signInPage.clickSignInButton();
		this.signInPage.clickSignInnerLink();
		this.signInPage.sendValidUserNamePassword();
		this.signInPage.clickSigninActionButton();	
	}	

	public void extentReportSetUp() throws IOException {
		if(this.getExtentReport() == null) {
			String testReportPath  = CommonUtilities.getPropertyValue("testreportpath");
			ExtentSparkReporter extentSparkReporter = new ExtentSparkReporter(testReportPath);
			extentSparkReporter.config().setReportName("Test Report");
			extentSparkReporter.config().setDocumentTitle("Test Result");
			
			this.setExtentReport(new ExtentReports());
			this.getExtentReport().attachReporter(extentSparkReporter);
			this.getExtentReport().setSystemInfo("Tester", "Tester Name");
		}
		
	}

	public void termination() {
		
		driver.close();
	}
	public ExtentReports getExtentReport() {
		return extentReport;
	}
	public void setExtentReport(ExtentReports extentReport) {
		this.extentReport = extentReport;
	}

}
