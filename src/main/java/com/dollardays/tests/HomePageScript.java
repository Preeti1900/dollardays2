package com.dollardays.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dollardays.common.BasePage;
import com.dollardays.common.BaseTest;
import com.dollardays.common.CommonUtilities;

public class HomePageScript extends BaseTest {

	public BasePage home;

	@BeforeMethod
	public void setup() throws IOException {

		initialization();
		home = new BasePage();

	}

	@Test
	public void verifyhomepage1Url() throws IOException, InterruptedException {
		this.signInMethod();
		String acttitle = CommonUtilities.getPropertyValue("url");
		String exptitle = driver.getCurrentUrl();
		Assert.assertEquals(exptitle, acttitle);
	}

	@AfterTest
	public void terminateBrowser() throws InterruptedException {
		Thread.sleep(3000);
		termination();
	}

}
